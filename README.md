# Tartan

Tartan is a plugin for the [Clang static analyser](http://clang-analyzer.llvm.org/) and C compiler which adds tighter support for GLib and GNOME, introducing extra checks and warnings when compiling C code which uses GLib. It was formerly known as ‘gnome-clang’, but renamed to be less GNOME-centric.

The plugin works by loading gobject-introspection metadata for all functions it encounters (both functions exported by the code being compiled, if it is a library; and functions called by it). This metadata is then used to add compiler attributes to the code, such as non-NULL attributes, which the compiler can then use for static analysis and emitting extra compiler warnings.

The plugin also detects common coding practices for code which uses GLib, such as the use of [`g_return_if_fail()`](https://developer.gnome.org/glib/stable/glib-Warnings-and-Assertions.html#g-return-if-fail) to describe function preconditions, and uses this to add extra compiler attributes.

## Dependencies

- glib-2.0 ≥ 2.38.0
- gio-2.0 ≥ 2.38.0
- gobject-introspection-1.0 ≥ 1.38.0
- llvm and clang ^16.0 - ^18.0
- meson ≥ 0.58.0

## Download and installation

You can obtain the latest version by cloning the [git repository](https://gitlab.freedesktop.org/tartan/tartan). Compilation follows the standard Meson process:

```
git clone https://gitlab.freedesktop.org/tartan/tartan.git
cd tartan
meson setup _build --prefix=$PREFIX
ninja -C _build
```

This will install the Tartan plugin, a dynamically loadable library, in `$PREFIX/lib/tartan/$LLVM_VERSION/libtartan.so`.

## Usage

Tartan can be used as a standalone plugin with the Clang static analyser, as a wrapper tool with Meson (this is the recommended way of using it), or can be integrated with JHBuild to be automatically run on all C source code compiled using JHBuild.

In both cases, all GIR typelibs installed on the system will be loaded automatically.

### Usage with Meson

To use with Meson, as per [instructions in the Meson manual](https://mesonbuild.com/howtox.html#use-clang-static-analyzer) run the following ([make sure to use the absolute path](https://github.com/mesonbuild/meson/issues/9744)):

```
SCANBUILD=$(which tartan-build) ninja -C _build/ scan-build
```

You will need to have `clang` and `scan-build` installed.

### Usage with JHBuild

[JHBuild](https://developer.gnome.org/jhbuild/stable/introduction.html.en) has in-built support for compiling with the `scan-build` wrapper, a script which comes with Clang to allow projects to be statically analysed in parallel with compilation (and without requiring Clang to be used for compilation itself). To enable this using Tartan, add the following lines to `~/.jhbuildrc`:

```
static_analyzer = True
static_analyzer_template = 'tartan-build -v -o %(outputdir)s/%(module)s
```

where `tartan-build` is a wrapper script for Clang which loads and enables the Tartan plugin. It can be used both installed (`$JHBUILD_PREFIX/bin/tartan`) or uninstalled (`/path/to/tartan/source/scripts/tartan`).

The `scan-build` wrapper can be used instead to use the Clang static analyzer without Tartan:

```
static_analyzer = True
static_analyzer_template = 'scan-build -v -o %(outputdir)s/%(module)s
```

This will enable static analysis of C code compiled with JHBuild, but will not enable GLib-specific warnings.

### Standalone usage

#### Automatic wrapper script

The easiest way to use Tartan standalone is with the `scan-build` script which comes with Clang. Tartan provides a wrapper script, `tartan-build`, which passes the right arguments to `scan-build`:

```
tartan-build -v ./autogen.sh
tartan-build -v make
```

where `./autogen.sh` is the command to configure your project and `make` builds it.

You can think of `tartan-build` as setting the `CC` environment variable to use the static analyser, so that `autogen.sh` picks that up.

The `tartan` script can be used installed or uninstalled, straight from the Tartan source directory.

#### Directly with Clang

To use Tartan with the Clang static analyser directly, use the `tartan` wrapper script by itself. The old command

```
clang -cc1 -analyze -std=c89 $system_includes my-project/*.c
```

becomes:

```
tartan -cc1 -analyze -std=c89 $system_includes my-project/*.c
```

which wraps the normal Clang compiler and automatically loads the correct plugin. The system includes should be set using:

```
system_includes=`echo | cpp -Wp,-v 2>&1 | \
grep '^[[:space:]]' | sed -e 's/^[[:space:]]*/-isystem/' | \
tr "\n" ' '`
```

You should also specify your project’s include flags, for example using `pkg-config`:

```
tartan -cc1 -analyze -std=c89 $system_includes `pkg-config --cflags glib-2.0` my-project/*.c
```

You can think of the `tartan` script as equivalent to `clang` or `gcc` — it acts as a compiler.

The `tartan` script looks for the `clang` binary in the same directory as itself, and then looks for `libtartan.so` in the same prefix. If either of these are installed in non-standard locations, set the `TARTAN_CLANG_CC` and `TARTAN_CLANG_PLUGIN` environment variables to the absolute paths of the `clang` binary and the `libtartan.so` library, respectively.

The `TARTAN_CLANG_CFLAGS` environment variable may optionally be set to pass extra options to the compiler after the Tartan options. Similarly, `TARTAN_CLANG_OPTIONS` may optionally be set to pass extra options to the Tartan plugin (only).

#### Raw Clang plugin

Finally, Tartan may be used without the wrapper script by passing the `-load`, `-add-plugin` and `-analyzer-checker` arguments directly to the compiler:

```
clang -cc1 -load $PREFIX/lib/tartan/$LLVM_VERSION/libtartan.so -add-plugin tartan -analyzer-checker tartan -analyze -std=c89 my-project/*.c
```

Again, Tartan can be used both while installed or uninstalled.

## Troubleshooting

If it seems like Tartan is not successfully being configured with use when building your project, there are a couple of things you can try to debug the situation.

 * Look at `config.log` (generated by `configure`) and see if there are any error messages related to ‘clang’ or ‘tartan’. If so, they should be fixed before proceeding.
 * Grep the compilation output for ‘[tartan]’, which prefixes all compiler errors and warnings outputted by Tartan. If any such message is present, Tartan is working.
 * Purposefully introduce an error in your code which Tartan will detect, such as passing `NULL` as the input string to `g_ascii_strtod()`, or using an invalid GVariant format string in a GVariant method. If this causes a ‘[tartan]’ error message, then Tartan is working. Otherwise it is not, and you should [contact](#contact) the authors.

## Source code and bugs

### Git repository

The source code for Tartan is stored in git, which can be [viewed online](https://gitlab.freedesktop.org/tartan/tartan) or cloned:

```
git clone https://gitlab.freedesktop.org/tartan/tartan.git
```

### Bugs

Tartan uses [GitLab on freedesktop.org for bug tracking and feature requests](https://gitlab.freedesktop.org/tartan/tartan).

To file a bug, please [file an issue on GitLab](https://gitlab.freedesktop.org/tartan/tartan/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

## Licensing

Tartan is distributed under the terms of the GNU General Public License,
version 3 or later. See the [COPYING](COPYING) file for details.

## Contact

- Philip Withnall <philip@tecnocode.co.uk>
- Website: http://www.freedesktop.org/software/tartan/
